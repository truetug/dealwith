from django.apps import apps, AppConfig
from django.db.models.signals import post_save


def org_post_save(sender, instance, created, **kwargs):
    if created or not instance.units.exists():
        Unit = apps.get_model('org.Unit')
        Unit.objects.create(
            org=instance,
            parent=None,
            name=instance.name,
        )


class OrgConfig(AppConfig):
    name = 'org'

    def ready(self):
        Org = self.get_model('Org')

        post_save.connect(org_post_save, sender='org.Org')
