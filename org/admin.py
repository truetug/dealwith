from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin

from org.models import Org, Unit, Employee


admin.site.register(Org)
admin.site.register(Unit, DraggableMPTTAdmin, list_filter=('org',))
admin.site.register(Employee, filter_horizontal=('units',), raw_id_fields=('user', ))
