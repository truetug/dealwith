from django.db import models
from django.conf import settings
from django.utils.translation import ugettext as _

from mptt.models import MPTTModel, TreeForeignKey, TreeManyToManyField


class Org(models.Model):
    name = models.CharField(_('name'), max_length=255)

    class Meta:
        verbose_name = _('organization')
        verbose_name_plural = _('organizations')

    def __str__(self):
        return self.name


class Unit(MPTTModel):
    org = models.ForeignKey(
        Org, related_name='units',
        verbose_name=_('organization')
    )
    name = models.CharField(_('name'), max_length=255)
    parent = TreeForeignKey(
        'self', related_name='children', verbose_name=_('parent'),
        null=True, blank=True, db_index=True
    )
    chief = models.ForeignKey(
        'Employee', related_name='people',
        verbose_name=_('chief'),
        null=True, blank=True
    )

    class Meta:
        verbose_name = _('unit')
        verbose_name_plural = _('units')

    def __str__(self):
        return self.name


class Employee(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='employees',
        verbose_name=_('user')
    )
    units = TreeManyToManyField(
        Unit, related_name='employees',
        verbose_name=_('units'),
        blank=True,
    )

    class Meta:
        verbose_name = _('employee')
        verbose_name_plural = _('employees')

    def __str__(self):
        return self.user.get_full_name()
