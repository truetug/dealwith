"""application URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.apps import apps

from rest_framework import routers, serializers, viewsets


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = apps.get_model(settings.AUTH_USER_MODEL)
        fields = ('email', 'is_staff')


class UserViewSet(viewsets.ModelViewSet):
    queryset = apps.get_model(settings.AUTH_USER_MODEL).objects.all()
    serializer_class = UserSerializer


class PostSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = apps.get_model('network.Post')
        fields = ('pk', 'content', 'created_by', 'created_at', 'is_published')


class PostViewSet(viewsets.ModelViewSet):
    queryset = apps.get_model('network.Post').objects.all()
    serializer_class = PostSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        qs = super(PostViewSet, self).get_queryset()

        content = self.request.GET.get('content')
        if content:
            qs = qs.filter(content__icontains=content)

        return qs


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'posts', PostViewSet)


urlpatterns = [
    url(r'^network/', include('network.urls', 'network')),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', 'rest_framework')),
    url(r'^admin/', admin.site.urls),
    url(r'', include('core.urls', 'core')),
    url(r'', include('social.apps.django_app.urls', 'social')),
]


urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if 'debug_toolbar' in settings.INSTALLED_APPS:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
