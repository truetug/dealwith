var autoprefixer = require('autoprefixer'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    path = require('path');

var prefix = path.resolve(__dirname, 'front/static/app'),
    src = path.resolve(prefix, 'src'),
    dist = path.resolve(prefix, 'dist'),
    extractSASS = new ExtractTextPlugin('bundle.css');

module.exports = {
    // entry: ['webpack/hot/dev-server', src + "/app.jsx"],
    resolve: {
      root: path.resolve(__dirname, 'front/static/app/src'),
      extensions: ['', '.js', '.jsx']
    },
    entry: src + '/app.jsx',
    output: {
        path: dist,
        filename: "bundle.js"
    },
    plugins: [
        extractSASS
    ],
    module: {
        loaders: [
            {
                test   : /\.(png|jpg)$/,
                loader : 'url?limit=8192&name=img/[hash].[ext]'
            },
            {
                test   : /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                loader : 'file?name=oth/[hash].[ext]'
            },
            {
                test: /.jsx?$/,
                loader: 'babel',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react', 'stage-0']
                }
            },
            {
                test: /\.(sass|scss)$/,
                loader: extractSASS.extract(
                    'style-loader',
                    [
                        'css',
                        'postcss',
                        'sass?indentedSyntax=sass&includePaths[]=' + src
                    ].join('!'))
            },
            {
                test: /\.css$/,
                loader: [
                    'style',
                    'css',
                    'postcss',
                ].join('!')
            }
        ]
    },
    postcss: [
        autoprefixer({
            browsers: ['last 2 versions']
        })
    ],
    node: {
        net: "empty",
        tls: "empty"
    }
};
