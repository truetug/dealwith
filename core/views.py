from django.shortcuts import render
from django.http import HttpResponse
from django import forms
from django.views.generic import DetailView, UpdateView
from django.core.paginator import Paginator

from .models import User
from network.models import Post


LAST_POSTS_COUNT = 3
POSTS_PAGINATE_BY = 5


def front_page(request):
    qs = Post.objects.filter(is_published=True)
    qs = qs.select_related('created_by')
    qs = qs[:LAST_POSTS_COUNT]
    return render(request, 'core/front_page.html', {
        # show 3 last posts even if react doesnt work
        'post_list': qs,
    })


def spa(request):
    return render(request, 'spa.html')


class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'gender')


class UserDetailView(UpdateView, DetailView):
    queryset = User.objects.all()
    form_class = UserUpdateForm
    slug_field = 'username'
    template_name = 'core/user_detail.html'

    def get_context_data(self, **kwargs):
        context = super(UserDetailView, self).get_context_data(**kwargs)

        # add pagination for posts
        post_list = self.object.created_posts.all()
        post_list = post_list.select_related('created_by')
        post_paginator = Paginator(post_list, POSTS_PAGINATE_BY)
        current_page = self.request.GET.get('page', 1)
        post_page = post_paginator.page(current_page)
        context.update({
            'post_list': post_list,
            'post_paginator': post_paginator,
            'post_page': post_page,
        })

        return context
