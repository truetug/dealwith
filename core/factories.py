import factory
from factory.fuzzy import FuzzyText


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'core.User'  # Equivalent to ``model = myapp.models.User``
        django_get_or_create = ('email', )

    # username =
    # first_name = factory.Sequence(lambda n: "Agent %03d" % n)
    # group = factory.SubFactory(GroupFactory)
