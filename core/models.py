from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext as _
from django.utils import timezone
from django.urls import reverse


class User(AbstractUser):
    GENDER_MALE = 'm'
    GENDER_FEMALE = 'f'

    GENDER_CHOICES = (
        (GENDER_MALE, _('Male')),
        (GENDER_FEMALE, _('Female')),
    )

    email = models.EmailField(
        _('email address'),
        unique=True
    )
    gender = models.CharField(
        _('Gender'), max_length=1,
        choices=GENDER_CHOICES
    )
    avatar = models.ImageField(
        _('Avatar'), upload_to='avatars/',
        null=True, blank=True,
    )

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        ordering = ('-date_joined', )

    def get_absolute_url(self):
        return reverse('core:user_detail', args=(self.username, ))

    def save(self, *args, **kwargs):
        return super(User, self).save(*args, **kwargs)
