import time

from core.models import User
from celery.task import task, periodic_task
from celery.schedules import crontab


@task
def add(x, y):
    result = x + y
    print('RESULT:', result)
    return result


@periodic_task(soft_time_limit=10, run_every=20)
def sample():
    time.sleep(11)
    print(User.objects.all())
