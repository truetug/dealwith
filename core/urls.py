from django.conf.urls import url

from .views import front_page, spa, UserDetailView


urlpatterns = [
    url('^$', front_page, name='frontpage'),
    url('^spa/$', spa, name='spa'),
    url('^profile/(?P<slug>[^/]+)/$', UserDetailView.as_view(), name='user_detail'),
]
