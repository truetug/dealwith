from django.test import TestCase, override_settings
from django.http import HttpRequest

from core.views import front_page


class FrontPageTest(TestCase):
    def test_front_page_returns_correct_html(self):
        request = HttpRequest()
        response = front_page(request)
        content = response.content.decode('utf-8').strip()

        self.assertIn('<title>DealWith</title>', content)
        self.assertTrue(content.startswith('<!DOCTYPE html>'))
        self.assertTrue(content.endswith('</html>'))


class CustomUserTest(TestCase):
    @override_settings(USER_AUTH_MODEL='core.models.User')
    def test_user_creation(self):
        # Попробуем создать юзера из кастомной модели

        # Попробуем изменить юзера

        # Залогиниться

        # Установить/сбросить пароль

        # Удалить


        self.fail('Finish the test')
