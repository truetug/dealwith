from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import User


@admin.register(User)
class UserAdmin(BaseUserAdmin):
	# list_display = (
	# 	'email', 'get_full_name', 'date_joined',
	# )

	ordering = ('-date_joined', )
