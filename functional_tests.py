#!/usr/bin/env python
# encoding: utf-8
import unittest

from selenium import webdriver


class FrontPageTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()

    def teatDown(self):
        self.browser.quit()

    def test_front_page(self):
        # Чувак услышал о новом крутом сайте

        # Он заходит на него
        self.browser.get('http://127.0.0.1:8000')

        # Он замечает, что в заголовке присутствует название
        self.assertIn('Django', self.browser.title)

        # Блаблабла
        self.fail('Do the test!')


if __name__ == '__main__':
    unittest.main(warnings='ignore')
