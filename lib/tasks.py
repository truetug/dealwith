from functools import wraps

from django.apps import apps
from django.core.cache import cache
from django.db import transaction
from celery import task
from celery.task import periodic_task
from celery.utils.log import get_task_logger
from celery.schedules import crontab

from .models import UndeletableAbstract

logger = get_task_logger(__name__)

GC_PERIODICITY = {'day': '*', 'hour': '4'}
DELETED_TIMEOUT = 60 * 60 * 24 * 365 / 2

@periodic_task(crontab(GC_PERIODICITY))
def gc():
    undeletable_model_list = (
        x for x in apps.get_models()
        if issubclass(x, UndeletableAbstract)
    )

    for model in undeletable_model_list:
        result = model.objects.deleted().delete(real=True)
        logger.info('Deleted\n%s', result)


class cache_lock(object):
    def __init__(self, func):
        self.key = 'task-{}'.format(func.__name__)

    def acquire(self, timeout=60 * 5):
        return cache.add(self.key, True, timeout=timeout)

    def release(self):
        return cache.delete(self.key)

    def __enter__(self):
        self.is_acuqired = self.acquire()
        return self.is_acuqired

    def __exit__(self, *args):
        if self.is_acuqired:
            self.release()


def lock_task(func, **kwargs):
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = None
        with cache_lock(func, *args, **kwargs) as is_acquire:
            if is_acquire:
                logger.debug('Lock for task %s is acquired', func)
                result = func(*args, **kwargs)
            else:
                logger.warning('Lock for task %s is not acquired', func)

        return result

    result = task(wrapper, **kwargs)
    return result


def atomic_task(func, **kwargs):
    @wraps(func)
    def wrapper(*args, **kwargs):
        with transaction.atomic():
            result = func(*args, **kwargs)

        return result

    result = task(wrapper, **kwargs)
    return result


@lock_task
def sample_one():
    logger.info('Freeze for 10 seconds')
    import time
    time.sleep(10)
    logger.info('Unfreeze')
    return True


@task(run_every={'minutes': 5})
def samle_schedule():
    logger.info('Iam scheduled')


@atomic_task
def sample_atomic(name=None):
    logger.info('Start of transaction')
    sample_model = apps.get_model('lib.Sample')
    sample_model.objects.create(name='test atomic')
    sample_model.objects.create(name=name)
    logger.info('End of transaction')


@task
def sample_nonatomic(name=None):
    sample_model = apps.get_model('lib.Sample')
    sample_model.objects.create(name='test nonatomic')
    sample_model.objects.create(name=name)
