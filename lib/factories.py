import factory
from factory.fuzzy import FuzzyText


class SampleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'lib.Sample'

    name = FuzzyText(
        length=20, prefix='sample_',
        chars='abcdefghijklmnoprstu_',
    )


class SampleItemFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'lib.SampleItem'

    name = FuzzyText(length=100)
    sample = factory.SubFactory(SampleFactory)


class ParentSampleFactory(SampleFactory):
    # parent = factory.SubFactory(SampleFactory)
    children = factory.RelatedFactory(SampleFactory, 'parent')
    items = factory.RelatedFactory(SampleItemFactory, 'sample')
