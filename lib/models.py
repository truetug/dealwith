from django.db import models, router
from django.db.models.deletion import Collector
from django.utils.translation import ugettext as _


class UndeletableCollector(Collector):
    def fake_delete(self):
        pass


class UndeletableQuerySet(models.QuerySet):
    def get_queryset(self):
        qs = super(UndeletableQuerySet, self).get_queryset()
        qs = qs.exclude(is_deleted=True)
        return qs

    def with_deleted(self):
        qs = super(UndeletableQuerySet, self).get_queryset()
        return qs

    def delete(self):
        """
        Deletes the records in the current QuerySet.
        """
        assert self.query.can_filter(), \
            "Cannot use 'limit' or 'offset' with delete."

        if self._fields is not None:
            raise TypeError("Cannot call delete() after .values() or .values_list()")

        del_query = self._clone()

        # The delete is actually 2 queries - one to find related objects,
        # and one to delete. Make sure that the discovery of related
        # objects is performed on the same database as the deletion.
        del_query._for_write = True

        # Disable non-supported fields.
        del_query.query.select_for_update = False
        del_query.query.select_related = False
        del_query.query.clear_ordering(force_empty=True)

        collector = Collector(using=del_query.db)
        collector.collect(del_query)
        deleted, _rows_count = collector.delete()

        # Clear the result cache, in case this QuerySet gets reused.
        self._result_cache = None
        return deleted, _rows_count


class UndeletableAbstract(models.Model):
    is_deleted = models.BooleanField(_('is deleted?'), default=False)

    def delete(self, using=None, keep_parents=False):
        """
        Делаем вид, что это реально удаление
        А на самом деле просто помечаем объект в базе, как удаленный
        Нужно возвращать все аффекченные объекты как реальный делит
        Нужно слать сигналы
        А в кверисете тоже надо все подменить
        И еще сделать ручку для реального удаления
        Ну или продумать эту тему получше
        """
        using = using or router.db_for_write(self.__class__, instance=self)
        collector = Collector(using=using)
        collector.collect([self], keep_parents=keep_parents)

        self._meta.model.objects.filter(pk=self.pk).update(is_deleted=True)

        # import ipdb; ipdb.set_trace()
        for key, values in collector.data.items():
            print(key, values)


        # signall

        stats = {}
        return collector.data

    objects = UndeletableQuerySet.as_manager()

    class Meta:
        abstract = True


class Sample(UndeletableAbstract):
    name = models.CharField(_('name'), max_length=255)
    parent = models.ForeignKey(
        'self', related_name='children',
        null=True, blank=True,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name


class SampleItem(UndeletableAbstract):
    name = models.CharField(_('name'), max_length=255)
    sample = models.ForeignKey('Sample', related_name='items')

    def __str__(self):
        return self.name
