from django.test import TestCase
from django.apps import apps
from django.db.utils import IntegrityError

from .tasks import sample_one, sample_atomic
from .factories import SampleFactory, ParentSampleFactory


class UndeletableTest(TestCase):
    def setUp(self):
        self.model = apps.get_model('lib.Sample')

    # Main test
    def test_model(self):
        # Создать запись и проверить
        # что она НЕ удаленная
        root = SampleFactory()
        SampleParentFactory.create_batch(2, sample=root)
        # todo: добавить ещё уровень или типа того
        self.assertFalse(
            root.is_deleted,
            msg='"is_delete" is set'
        )

        # Удалить и проверить
        # что запись удаленная
        # что дети тоже удалены
        delete_result = self.instance.delete()
        self.assertTrue(
            root.is_deleted,
            msg='"is_delete" is NOT set'
        )

        qs = root.children.filter(is_deleted=True)
        self.assertTrue(
            qs.exists(),
            msg='children are not is_deleted'
        )

        qs = qs.first().items.filter(is_deleted=True)
        self.assertTrue(
            qs.exists(),
            msg='items are not is_deleted'
        )

        # Проверить, что удаленные не попадают
        # в обычные запросы
        qs = self.model.objects.all()
        self.assertFalse(
            qs.exists(),
            msg='deleted are visible'
        )

        # Удалить и проверить,
        # что реальное удаление тоже работает
        real_delete_result = root.delete(real=True)
        self.assertEqual(
            root, None,
            msg='real deletion does not work'
        )

        # Проверяем, что данные по удалениям совпадают
        self.assertEqualDict(
            delete_result, real_delete_result,
            msg="results of real and fake delition are not equal"
        )

    def test_queryset(self):
        root = SampleFactory()
        SampleParentFactory.create_batch(2)

        # Проверить, что удаление из кверисета
        # проставляет записям флаг удаления
        self.model.all().delete()
        self.assertTrue(root.is_deleted)

        # Проверить, что настоящее удаление работает
        self.model.all().delete(real=True)


class CeleryTest(TestCase):
    def setUp(self):
        self.has_workers = False

    def test_atomic(self):
        try:
            sample_atomic()
        except IntegrityError:
            pass

        self.assertFalse(
            apps.get_model('lib.Sample').objects.all().exists()
        )

    def test_lock(self):
        """
        It's just an example
        it does not work in unit test
        without at least 2 workers
        """
        if self.has_workers:
            result_1 = sample_one.delay()
            result_2 = sample_one.delay()
            self.assertEqual(result_1, True)
            self.assertEqual(result_2, None)
        else:
            pass
