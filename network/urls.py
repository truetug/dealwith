from django.conf.urls import url

from .views import PostDetail, PostList, PostCreate


urlpatterns = [
    url('^$', PostList.as_view(), name='list'),
    url('^(?P<pk>\d+)/$', PostDetail.as_view(), name='detail'),
    url('^add/$', PostCreate.as_view(), name='create'),
]
