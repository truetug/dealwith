from django.contrib import admin

from .models import Post, Comment


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
	list_display = ('__str__', 'is_published')
	list_filter = ('is_published', )
	search_fields = ('created_by__email', )
	list_editable = ('is_published', )


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
	list_display = ('__str__', 'parent', 'content_type_id', 'object_id')
