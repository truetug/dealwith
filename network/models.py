from django.db import models
from django.utils.translation import ugettext as _
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse


class CommonModel(models.Model):
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='created_%(class)ss',
        verbose_name=_('created by')
    )
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Post(CommonModel):
    heading = models.CharField(_('heading'), max_length=255)
    content = models.TextField(_('content'))
    is_published= models.BooleanField(_('is published'), default=True)

    class Meta:
        verbose_name = _('post')
        verbose_name_plural = _('posts')
        ordering = ('-created_at', )

    def __str__(self):
        return u'{}...'.format(self.heading[:30])

    def save(self, *args, **kwargs):
        return super(Post, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('network:detail', args=(self.pk, ))


class Comment(CommonModel):
    parent = models.ForeignKey(
        'self', related_name='children',
        verbose_name=_('parent'),
        blank=True, null=True,
    )
    content = models.TextField(_('content'))

    content_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE
    )
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        verbose_name = _('comment')
        verbose_name_plural = _('comments')
        ordering = ('-created_at', )
