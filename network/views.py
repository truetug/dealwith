from django import forms
from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView
from django.urls import reverse_lazy

from .models import Post


class PostForm(forms.ModelForm):
	class Meta:
		model = Post
		fields = ('heading', 'content')


class PostDetail(DetailView):
    model = Post
    queryset = Post.objects.all().select_related('created_by')


class PostCreate(CreateView):
	form_class = PostForm
	success_url = reverse_lazy('network:list')
	template_name = 'network/post_form.html'

	def form_valid(self, form):
		form.instance.created_by = self.request.user
		return super(PostCreate, self).form_valid(form)


class PostList(PostCreate, ListView):
    queryset = Post.objects.all().select_related('created_by')
    template_name = 'network/post_list.html'
    paginate_by = 5
