class Root extends React.Component {
  propTypes: {
    // autoPlay: React.PropTypes.bool.isRequired,
    // maxLoops: React.PropTypes.number.isRequired,
    // posterFrameSrc: React.PropTypes.string.isRequired,
    // videoSrc: React.PropTypes.string.isRequired,
  }

  constructor (props) {
    super(props);

    this.state = {url: '', text: ''};
  }

  handleUrlChange (e) {
    this.setState({url: e.target.value});
  }

  loadDataFromServer (e) {
    e.preventDefault();
    var _this = this;
    var url = this.state.url || '/';

    fetch(url, {
      credentials: 'include',
    })
      .then(
        function(response) {
          if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' +
              response.status);

            _this.setState({text: '<p>' + response.status + '</p>'});
          }

          // Examine the text in the response
          // response.json().then(function(data) {
          //   console.log(data);
          // });

          response.text().then(function(data){
            // console.log('Received text', data);
            var kind = 'json';

            try {
              data = JSON.parse(data);
            } catch(err) {
              kind = 'text'
              console.log('It is not JSON', err);
            }

            _this.setState({text: data, kind: kind});
          })
        }
      )
      .catch(function(err) {
        console.log('Fetch Error :-S', err);
        _this.setState({text: '<p>' + err + '</p>'});
      });
  }

  render () {
    let style = {
      width: '40em',
      border: '1px solid #CCC',
      borderRadius: '2px',
      overflow: 'auto',
    },
    year = (new Date()).getFullYear();

    return (
      <div className="b-root">
        <nav>
          <div className="nav-wrapper">
            <a href="#" className="brand-logo">Logo</a>
            <ul id="nav-mobile" className="right hide-on-med-and-down">
              <li><a href="sass.html">Sass</a></li>
              <li><a href="badges.html">Components</a></li>
              <li><a href="collapsible.html">JavaScript</a></li>
            </ul>
          </div>
        </nav>
        <div className="row">
          <form className="col s12" onSubmit={this.loadDataFromServer.bind(this)}>
            <div className="row">
              <div className="input-field col s6">
                <i className="material-icons prefix">account_circle</i>
                <input id="id_url" type="text" size="60" value={this.state.url} onChange={this.handleUrlChange.bind(this)} placeholder="Enter a local url" className="validate" />
                <label htmlFor="id_url">URL</label>
              </div>
            </div>
            <div className="row">
              <button className="btn waves-effect waves-light" type="submit" name="action">Submit
                <i className="material-icons right">send</i>
              </button>
            </div>
          </form>
        </div>
        <div className="row">
          <div className="col s12">
            <hr/>
          </div>
        </div>
        <div className="row">
          <div className="col s12">
            {(this.state.kind == 'json') ? <div style={style} dangerouslySetInnerHTML={{__html: (!this.state.text) ? '<p>Hello, World!</p>':this.state.text }}></div> : <Iframe html={this.state.text} />}
          </div>
        </div>
        <footer className="page-footer">
          <div className="container">
            <div className="row">
              <div className="col l6 s12">
                <h5 className="white-text">Footer Content</h5>
                <p className="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
              </div>
              <div className="col l4 offset-l2 s12">
                <h5 className="white-text">Links</h5>
                <ul>
                  <li><a className="grey-text text-lighten-3" href="#!">Link 1</a></li>
                  <li><a className="grey-text text-lighten-3" href="#!">Link 2</a></li>
                  <li><a className="grey-text text-lighten-3" href="#!">Link 3</a></li>
                  <li><a className="grey-text text-lighten-3" href="#!">Link 4</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div className="footer-copyright">
            <div className="container">
            © <a href="http://truetug.info">tug</a>, {year}
            <a className="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
        </footer>
      </div>
    )
  }
}

ReactDOM.render(
  <Root />,
  document.getElementById('content')
);
