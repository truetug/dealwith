import React from 'react';
import ReactDOM from 'react-dom';

// import 'bootstrap-sass/assets/stylesheets/_bootstrap.scss';
// import './styles.sass';

import Root from 'components/root';


const config = {
  clientId: '4a7ba422f281159ce5df0bdee59d700b',
  playlistId: '1289428',
}

try {
  let container = document.querySelector('.b-wall');
  ReactDOM.render(<Root config={config}/>, container);
} catch(err) {
  console.log('No proper container', err);
};
