import React from 'react';
import PostList from 'components/postlist';

import MjmlEditor from 'components/mjml';
import centrifuge from 'lib/centrifuge';


export default class Root extends React.Component {
  render(){
    return (
      <div>
        <PostList />

        <MjmlEditor />
      </div>
    )
  }
}
