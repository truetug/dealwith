import React from 'react';


export default class Post extends React.Component {
	state = {
		isOpen: false,
	}

	showContent(e) {
		this.setState({isOpen: !this.state.isOpen});
	}

	render () {
		let style = {
			border: '4px solid #CCC',
			padding: '1em',
			background: (this.props.is_published) ? '#FFF' : '#EFEFEF',
		}

		return (
			<p style={style} onClick={this.showContent.bind(this)}>
				{(this.state.isOpen) ? this.props.content : this.props.content.slice(0, 100)}
			</p>
		)
	}
}
