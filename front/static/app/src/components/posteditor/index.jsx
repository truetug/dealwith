import React from 'react';


export default class PostEditor extends React.Component {
	handleSubmit(e) {
		e.preventDefault();
		console.log('DDD');
	}

	render() {
		return (
			<div className="b-post-editor">
				<form onSubmit={this.handleSubmit.bind(this)}>
					<input type="text" id="id_title" name="title" value={this.state.title} />
					<textarea id="id_content" name="content">{this.state.content}</textarea>
					<button type="submit">Save</button>
				</form>
			</div>
		)
	}
}
