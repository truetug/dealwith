import React from 'react';
import update from 'react-addons-update';

import Post from 'components/post';
import PostSearch from 'components/postsearch';
import 'components/postlist/styles.css';

import { centrifuge } from 'lib/centrifuge';


export default class PostList extends React.Component {
	state = {
		objects: [],
	}

	onSearch(content) {
		// По набору в инпут
		this.fetchDataFromServer(content);
	}

	componentDidMount() {
		// После первой отрисовки компонента
		this.fetchDataFromServer();

		const _this = this;
		centrifuge.subscribe('postlist', function(message) {
				let objects = update(_this.state.objects, {$push: [message.data]});
				console.log(message, objects);
		    _this.setState({
					objects: objects
				})
		});
	}

	loadDataFromServer(e) {
		// Для кнопочки
		this.fetchDataFromServer();
	}

	fetchDataFromServer(content) {
		let _this = this,
				url = '/api/posts/';

		if(content) url = url + '?content=' + content;

		fetch(url)
			.then(function(response){
				console.log(response);
				response.json().then(function(data){
					console.log(data);
					_this.setState({objects: data});
				})
			})
			.catch(function(err){
				console.log('Error', err);
			})
	}

	render () {
		return (
			<div>
				<PostSearch
					onButtonClick={this.loadDataFromServer.bind(this)}
					onSearch={this.onSearch.bind(this)} />
				{this.state.objects.map((item) => <Post key={`post_${item.pk}`} {...item} />)}
			</div>
		)
	}
}
