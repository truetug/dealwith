import React from 'react';
// import MJMLRenderer from 'mjml/lib';


export default class MjmlEditor extends React.Component {
  state = {
    content: '' +
      '<mjml>' +
        '<mj-body>' +
          '<mj-container>' +
            '<mj-section>' +

              '<mj-column>' +
                '<mj-table>' +
                  '<tr style="border-bottom:1px solid #ecedee;text-align:left;padding:15px 0;">' +
                    '<th style="padding: 0 15px 0 0;">Year</th>' +
                    '<th style="padding: 0 15px;">Language</th>' +
                    '<th style="padding: 0 0 0 15px;">Inspired from</th>' +
                  '</tr>' +
                  '<tr>' +
                    '<td style="padding: 0 15px 0 0;">1995</td>' +
                    '<td style="padding: 0 15px;">PHP</td>' +
                    '<td style="padding: 0 0 0 15px;">C, Shell Unix</td>' +
                  '</tr>' +
                  '<tr>' +
                    '<td style="padding: 0 15px 0 0;">1995</td>' +
                    '<td style="padding: 0 15px;">JavaScript</td>' +
                    '<td style="padding: 0 0 0 15px;">Scheme, Self</td>' +
                  '</tr>' +
                '</mj-table>' +
              '</mj-column>' +
            '</mj-section>' +
          '</mj-container>' +
        '</mj-body>' +
      '</mjml>'
  }

  handleChange(e) {
    this.setState({content: e.target.value});
  }

  render() {
    // let value = new MJMLRenderer(this.state.content, {level: strict});
    let value = this.state.content;

    return (
      <div>
        <textarea value={value} onChange={this.handleChange.bind(this)}></textarea>
      </div>
    )
  }
}
