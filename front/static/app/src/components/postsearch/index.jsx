import React from 'react';


export default class PostSearch extends React.Component {
	state = {
		content: '',
	}

	onChangeContent (e) {
		this.setState({content: e.target.value});
		this.props.onSearch(e.target.value);
	}

	render () {
		return (
			<div>
				<label htmlFor="id_search">Search string</label>&nbsp;
				<input
					onChange={this.onChangeContent.bind(this)}
					id="id_search"
					type="text"
					size="30"
					value={this.state.content}
					placeholder="search for" />

				<button onClick={this.props.onButtonClick}>Go</button>
			</div>
		)
	}
}
