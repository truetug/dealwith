import React from 'react';


class Iframe extends React.Component {
  handleClick (e) {
    e.preventDefault();
  }

  render () {
    return (
      <div className="b-iframe">
        <iframe srcDoc={this.props.html} onClick={this.handleClick.bind(this)}></iframe>
      </div>
    )
  }
}
