import Centrifuge from 'centrifuge';
import SockJS from 'sockjs-client'

export var centrifuge = new Centrifuge({
    url: 'http://127.0.0.1:8001',
    user: '',
    timestamp: 'UNIX TIMESTAMP SECONDS',
    token: 'token',
    sockJS: SockJS,
});


// Sample
centrifuge.subscribe('chat', function(message) {
    console.log(message);
});

centrifuge.connect();
